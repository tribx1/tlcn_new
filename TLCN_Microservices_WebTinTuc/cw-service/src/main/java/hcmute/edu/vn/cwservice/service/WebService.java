package hcmute.edu.vn.cwservice.service;

import hcmute.edu.vn.cwservice.entity.Web;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface WebService {

    CrudRepository <Web, Long> getRepo();
    Page<Web> retrieveWebAll(Pageable pageable);
    Web retieveWebByID(Long id);
    Web retrieveWebByTitle(String webtitle);
    Web addWeb(Web web);
    Web updateWeb(Web web);
    Boolean deleteWeb(Long id);

}
