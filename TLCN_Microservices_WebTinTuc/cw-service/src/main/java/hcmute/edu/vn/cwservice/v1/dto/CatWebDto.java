package hcmute.edu.vn.cwservice.v1.dto;

import lombok.Data;

@Data
public class CatWebDto {
        private String catId;
    private String catName;
    private String webId;
    private String webTitle;
    private String rssLink;
}
