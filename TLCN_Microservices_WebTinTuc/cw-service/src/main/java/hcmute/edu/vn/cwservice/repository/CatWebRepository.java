package hcmute.edu.vn.cwservice.repository;

import hcmute.edu.vn.cwservice.entity.CatWeb;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CatWebRepository extends JpaRepository<CatWeb,Long> {
    Optional<CatWeb> findById_Cat_IdAndId_Web_Id(Long catId, Long webId);
    List<CatWeb> findById_Web_Id(Long webId);
}
