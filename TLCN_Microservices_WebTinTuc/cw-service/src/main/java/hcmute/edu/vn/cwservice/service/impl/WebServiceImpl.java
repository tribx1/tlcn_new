package hcmute.edu.vn.cwservice.service.impl;

import hcmute.edu.vn.cwservice.entity.CatWeb;
import hcmute.edu.vn.cwservice.entity.Web;
import hcmute.edu.vn.cwservice.exception.NotFoundException;
import hcmute.edu.vn.cwservice.repository.CatWebRepository;
import hcmute.edu.vn.cwservice.repository.WebRepository;
import hcmute.edu.vn.cwservice.service.WebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WebServiceImpl implements WebService {

    @Autowired
    WebRepository webRepository;

    @Autowired
    CatWebRepository catWebRepository;

    @Override
    public CrudRepository<Web, Long> getRepo() {
        return webRepository;
    }

    @Override
    public Page<Web> retrieveWebAll(Pageable pageable) {
        Page<Web> webs = webRepository.findAll(pageable);
        if (webs.isEmpty()) {
            return null;
        }
        return webs;
    }

    @Override
    public Web retieveWebByID(Long id) {
        return webRepository.findById(id).get();
    }

    @Override
    public Web retrieveWebByTitle(String webtitle) {
        Optional<Web> web = webRepository.findWebByTitle(webtitle);
        if(!web.isPresent()){
            throw new NotFoundException("Không tìm thấy title");
        }
        return web.get();
    }

    @Override
    public Web addWeb(Web web) {
        Optional<Web> webOptional = webRepository.findWebByTitle(web.getTitle());
        if(webOptional.isPresent()){
            throw new NotFoundException("Đã tồn tại");
        }
        return webRepository.save(web);
    }

    @Override
    public Web updateWeb(Web web) {
        Optional<Web> webOptional = webRepository.findById(web.getId());
        if(!webOptional.isPresent()){
            throw new NotFoundException("Không tìm thấy tại web");
        }
        web.setCatWeb(webOptional.get().getCatWeb());
        return webRepository.save(web);
    }

    @Override
    public Boolean deleteWeb(Long id) {
        Optional<Web> webOptional = webRepository.findById(id);
        if(!webOptional.isPresent()){
            throw new NotFoundException("Không tìm thấy tại web");
        }
        List<CatWeb> catWebs = catWebRepository.findById_Web_Id(id);
        catWebRepository.deleteAll(catWebs);
        webRepository.delete(webOptional.get());
        return true;
    }

}
