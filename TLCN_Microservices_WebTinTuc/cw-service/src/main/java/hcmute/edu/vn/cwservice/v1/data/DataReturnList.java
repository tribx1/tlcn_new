package hcmute.edu.vn.cwservice.v1.data;

import lombok.Data;

import java.util.List;

@Data
public class DataReturnList<T> {
    private String message;
    private String success="true";
    private List<T> data;
    private int totalPage;
    private long totalElement;
}
