package hcmute.edu.vn.cwservice.controller;

import hcmute.edu.vn.cwservice.entity.CatWeb;
import hcmute.edu.vn.cwservice.entity.Web;
import hcmute.edu.vn.cwservice.exception.NotFoundException;
import hcmute.edu.vn.cwservice.service.CatService;
import hcmute.edu.vn.cwservice.service.CatWebService;
import hcmute.edu.vn.cwservice.service.ItemService;
import hcmute.edu.vn.cwservice.service.WebService;
import hcmute.edu.vn.cwservice.v1.data.DataReturnList;
import hcmute.edu.vn.cwservice.v1.data.DataReturnOne;
import hcmute.edu.vn.cwservice.v1.dto.CatWebDto;
import hcmute.edu.vn.cwservice.v1.mapper.CatWebMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@RequestMapping("cw")
public class CrawlerController {

    @Autowired
    CatWebService catWebService;

    @Autowired
    CatService catService;

    @Autowired
    WebService webService;

    @Autowired
    ItemService itemService;

    @Autowired
    CatWebMapper catWebMapper;

    @PostMapping("/web")
    public DataReturnOne<Web> createWeb(@RequestBody Web web){
        DataReturnOne<Web> dataReturnOne = new DataReturnOne<>();
        try {
            Web web1 = webService.addWeb(web);
            dataReturnOne.setData(web1);
            dataReturnOne.setMessage("Create web success");
        } catch (NotFoundException e) {
            dataReturnOne.setMessage("Web đã tồn tại");
            dataReturnOne.setSuccess("false");
            dataReturnOne.setData(null);
        }
        return dataReturnOne;
    }

    @GetMapping("/web")
    public DataReturnList<Web> retieveWeb(@RequestParam int size, @RequestParam int page){
        DataReturnList<Web> dataReturnList = new DataReturnList<Web>();
        Pageable pageable = PageRequest.of(page, size);
        try {
            Page<Web> web = webService.retrieveWebAll(pageable);
            dataReturnList.setData(web.getContent());
            dataReturnList.setMessage("Lấy danh sách web thành công");
            dataReturnList.setTotalElement(web.getTotalElements());
            dataReturnList.setTotalPage(web.getTotalPages());
        } catch (NotFoundException e) {
            dataReturnList.setMessage("List Web không tồn tại");
            dataReturnList.setSuccess("false");
            dataReturnList.setData(null);
        }
        return dataReturnList;
    }

    @PutMapping("/web")
    public DataReturnOne<Web> updateWeb(@RequestBody Web web){
        DataReturnOne<Web> dataReturnOne = new DataReturnOne<>();
        try {
            Web web1 = webService.updateWeb(web);
            dataReturnOne.setData(web1);
            dataReturnOne.setMessage("Update web success");
        } catch (NotFoundException e) {
            dataReturnOne.setMessage("Không tìm thấy web");
            dataReturnOne.setSuccess("false");
            dataReturnOne.setData(null);
        }
        return dataReturnOne;
    }

    @DeleteMapping("/web")
    public DataReturnOne<Web> deleteWeb(@RequestParam Long webId){
        DataReturnOne<Web> dataReturnOne = new DataReturnOne<>();
        try {
            webService.deleteWeb(webId);
            dataReturnOne.setData(null);
            dataReturnOne.setMessage("Delete web success");
        } catch (NotFoundException e) {
            dataReturnOne.setMessage("Không tìm thấy web");
            dataReturnOne.setSuccess("false");
            dataReturnOne.setData(null);
        }
        return dataReturnOne;
    }

    @PostMapping("/rss")
    public DataReturnOne<CatWebDto> createRss(@RequestParam Long webId, @RequestParam Long catId, @RequestParam String rssLink){
        DataReturnOne<CatWebDto> dataReturnOne = new DataReturnOne<>();
        try {
            CatWeb catWeb = catWebService.createCatWebById(webId,catId,rssLink);
            CatWebDto catWebDto = new CatWebDto();
            catWebDto.setCatName(catWeb.getId().getCat().getName());
            catWebDto.setWebTitle(catWeb.getId().getWeb().getTitle());
            catWebDto.setRssLink(rssLink);
            dataReturnOne.setData(catWebDto);
            dataReturnOne.setMessage("Create cat web success");
        } catch (NotFoundException e) {
            dataReturnOne.setMessage(e.getMessage());
            dataReturnOne.setSuccess("false");
            dataReturnOne.setData(null);
        }
        return dataReturnOne;
    }

    @PutMapping("/rss")
    public DataReturnOne<CatWebDto> updateRss(@RequestParam Long webId, @RequestParam Long catId, @RequestParam String rssLink){
        DataReturnOne<CatWebDto> dataReturnOne = new DataReturnOne<>();
        try {
            CatWeb catWeb = catWebService.createCatWebById(webId,catId,rssLink);
            CatWebDto catWebDto = new CatWebDto();
            catWebDto.setCatName(catWeb.getId().getCat().getName());
            catWebDto.setWebTitle(catWeb.getId().getWeb().getTitle());
            catWebDto.setRssLink(rssLink);
            dataReturnOne.setData(catWebDto);
            dataReturnOne.setMessage("Update cat web success");
        } catch (NotFoundException e) {
            dataReturnOne.setMessage(e.getMessage());
            dataReturnOne.setSuccess("false");
            dataReturnOne.setData(null);
        }
        return dataReturnOne;
    }

    @GetMapping("/rss")
    public DataReturnList<CatWebDto> getAllCatWeb(@RequestParam int size, @RequestParam int page){
        DataReturnList<CatWebDto> dataReturnList = new DataReturnList<CatWebDto>();
        Pageable pageable = PageRequest.of(page, size);
        try {
            Page<CatWeb> catWeb ;
            catWeb = catWebService.retrieveAll(pageable);
            dataReturnList.setMessage("Get Cat Web Success");
            dataReturnList.setData(catWeb.getContent().stream().map(catWebMapper::catWebTolistCatWebDto).collect(Collectors.toList()));
            dataReturnList.setTotalElement(catWeb.getTotalElements());
            dataReturnList.setTotalPage(catWeb.getTotalPages());
        } catch (NotFoundException e) {
            dataReturnList.setMessage(e.getMessage());
            dataReturnList.setSuccess("false");
            dataReturnList.setData(null);
        }
        return dataReturnList;
    }

    @DeleteMapping("/rss")
    public DataReturnOne<String> deleteRss(@RequestParam Long webId, @RequestParam Long catId){
        DataReturnOne<String> dataReturnOne = new DataReturnOne<>();
        System.out.println("webId : " + webId + " catId : " + catId);
        try {
            Boolean catWeb = catWebService.deleteCatWebId(webId,catId);
            dataReturnOne.setData("");
            dataReturnOne.setMessage("Delete cat web success");
        } catch (NotFoundException e) {
            dataReturnOne.setMessage(e.getMessage());
            dataReturnOne.setSuccess("false");
            dataReturnOne.setData(null);
        }
        return dataReturnOne;
    }


}
