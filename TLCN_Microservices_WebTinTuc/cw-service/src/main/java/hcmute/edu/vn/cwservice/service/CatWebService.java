package hcmute.edu.vn.cwservice.service;

import hcmute.edu.vn.cwservice.entity.CatWeb;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CatWebService {
    CatWeb createCatWeb(CatWeb catWeb);
    Page<CatWeb> retrieveAll(Pageable pageable);
    List<CatWeb> retrieveAllNoPage();
    CatWeb createCatWebById(Long webId, Long catId, String rssLink);
    CatWeb updateCatWebById(Long webId, Long catId, String rssLink);
    Boolean deleteCatWebId(Long webId, Long catId);
}
