package hcmute.edu.vn.cwservice.service.impl;

import hcmute.edu.vn.cwservice.entity.Cat;
import hcmute.edu.vn.cwservice.entity.CatWeb;
import hcmute.edu.vn.cwservice.entity.CatWebId;
import hcmute.edu.vn.cwservice.entity.Web;
import hcmute.edu.vn.cwservice.exception.NotFoundException;
import hcmute.edu.vn.cwservice.repository.CatRepository;
import hcmute.edu.vn.cwservice.repository.CatWebRepository;
import hcmute.edu.vn.cwservice.repository.WebRepository;
import hcmute.edu.vn.cwservice.service.CatWebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CatWebServiceImpl implements CatWebService {

    @Autowired
    CatWebRepository catWebRepository;

    @Autowired
    CatRepository catRepository;

    @Autowired
    WebRepository webRepository;

    @Override
    public Page<CatWeb> retrieveAll(Pageable pageable) {
        Page<CatWeb> catWebs ;
        catWebs = catWebRepository.findAll(pageable);
        if(catWebs.isEmpty()) {
            throw new NotFoundException("List Cat Web not found");
        }
        return catWebs;
    }

    @Override
    public List<CatWeb> retrieveAllNoPage() {
        List<CatWeb> catWebs = new ArrayList<CatWeb>();
        catWebs = catWebRepository.findAll();
        if(catWebs.isEmpty()) {
            throw new NotFoundException("List Cat Web not found");
        }
        return catWebs;
    }

    @Override
    public CatWeb createCatWeb(CatWeb catWeb) {
        return catWebRepository.save(catWeb);
    }

    @Override
    public CatWeb createCatWebById(Long webId, Long catId, String rssLink) {
        Cat cat = new Cat();
        Web web = new Web();
        Optional<Cat> catOptional = catRepository.findById(catId);
        Optional<Web> webOptional = webRepository.findById(webId);
        if(!catOptional.isPresent()) {
            throw new NotFoundException("Cat not found");
        }
        if(!webOptional.isPresent()) {
            throw new NotFoundException("Web not found");
        }
        cat = catOptional.get();
        web = webOptional.get();
        CatWebId catWebId = new CatWebId(cat,web);
        CatWeb catWeb = new CatWeb(catWebId, rssLink);
        return catWebRepository.save(catWeb);
    }

    @Override
    public CatWeb updateCatWebById(Long webId, Long catId, String rssLink) {
        return null;
    }

    @Override
    public Boolean deleteCatWebId(Long webId, Long catId) {
        Optional<CatWeb> catWebOptional = catWebRepository.findById_Cat_IdAndId_Web_Id(catId,webId);
        if(!catWebOptional.isPresent()) {
            throw new NotFoundException("Cat Web not found");
        }
        catWebRepository.delete(catWebOptional.get());
        return true;
    }


}
