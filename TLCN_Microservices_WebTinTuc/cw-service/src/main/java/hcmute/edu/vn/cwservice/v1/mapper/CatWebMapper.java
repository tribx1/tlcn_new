package hcmute.edu.vn.cwservice.v1.mapper;

import hcmute.edu.vn.cwservice.entity.CatWeb;
import hcmute.edu.vn.cwservice.v1.dto.CatWebDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CatWebMapper {
    CatWebMapper INSTANCE = Mappers.getMapper(CatWebMapper.class);
    @Mappings({

            @Mapping(source = "id.cat.name", target = "catName"),
            @Mapping(source = "id.web.title", target = "webTitle"),
            @Mapping(source = "id.cat.id", target = "catId"),
            @Mapping(source = "id.web.id", target = "webId"),
            @Mapping(source = "rssLink", target = "rssLink")
    })
    CatWebDto catWebTolistCatWebDto(CatWeb catWeb);
}
