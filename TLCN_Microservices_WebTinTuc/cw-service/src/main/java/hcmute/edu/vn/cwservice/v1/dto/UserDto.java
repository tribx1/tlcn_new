package hcmute.edu.vn.cwservice.v1.dto;

import lombok.Data;

@Data
public class UserDto {
    private String email;
    private String password;
}
