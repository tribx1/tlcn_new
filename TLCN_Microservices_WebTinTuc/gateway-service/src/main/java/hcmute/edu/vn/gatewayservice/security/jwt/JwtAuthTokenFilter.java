package hcmute.edu.vn.gatewayservice.security.jwt;

import hcmute.edu.vn.gatewayservice.security.UserPrinciple;
import hcmute.edu.vn.gatewayservice.service.UserDetailsServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthTokenFilter extends OncePerRequestFilter {

	@Autowired
	private JwtProvider tokenProvider;

	@Autowired
	private UserDetailsServiceImpl userService;

	private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthTokenFilter.class);

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		try {
			if(request.getMethod().equalsIgnoreCase(HttpMethod.OPTIONS.name())) {
				LOGGER.info(request.getRequestURI());
				LOGGER.info("OPTIONS");
				String jwt = getJwt(request);
				LOGGER.info(jwt);
				filterChain.doFilter(request, response);
			} else {
				LOGGER.info(request.getRequestURI());
				String jwt = getJwt(request);
				LOGGER.info(jwt);
				if (jwt != null && tokenProvider.validateJwtToken(jwt)) {
					String email = tokenProvider.getUserNameFromJwtToken(jwt);

					UserPrinciple userDetails = userService.loadUserByUsername(email);
					UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
							userDetails, null, userDetails.getAuthorities());
					authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

					SecurityContextHolder.getContext().setAuthentication(authentication);
				}
			}

		} catch (Exception e) {
			LOGGER.error("Can NOT set user authentication -> Message: {}", e);
		
		}

		filterChain.doFilter(request, response);
	}

	private String getJwt(HttpServletRequest request) {
		String authHeader = request.getHeader("Authorization");

		if (authHeader != null && authHeader.startsWith("Bearer ")) {
			return authHeader.replace("Bearer ", "");
		}

		return null;
	}
}
