package hcmute.edu.vn.gatewayservice.api.v1.dto;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtResponseDto {
	private String lastName;
	private String token;
	private String type = "Bearer";
	private String email;	
	private Collection<? extends GrantedAuthority> authorities;
}
