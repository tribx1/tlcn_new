package hcmute.edu.vn.gatewayservice.service;

import hcmute.edu.vn.gatewayservice.api.v1.data.DataReturnOne;
import hcmute.edu.vn.gatewayservice.api.v1.dto.UserDto;
import hcmute.edu.vn.gatewayservice.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;


@FeignClient(name="nu-service" )
@RequestMapping(value = "/nuser")
public interface NoneUserServiceProxy {
	@GetMapping("/find-user/{email}")
	public DataReturnOne<User> checkUser(@PathVariable(value = "email") String email);
	
	@PostMapping("/register")
	public DataReturnOne<UserDto> register(@RequestBody User user);
}
