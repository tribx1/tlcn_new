package hcmute.edu.vn.gatewayservice.controller;

import hcmute.edu.vn.gatewayservice.api.v1.data.DataReturnOne;
import hcmute.edu.vn.gatewayservice.api.v1.dto.JwtResponseDto;
import hcmute.edu.vn.gatewayservice.api.v1.dto.UserDto;
import hcmute.edu.vn.gatewayservice.entity.User;
import hcmute.edu.vn.gatewayservice.security.UserPrinciple;
import hcmute.edu.vn.gatewayservice.security.jwt.JwtProvider;
import hcmute.edu.vn.gatewayservice.service.NoneUserServiceProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@CrossOrigin(origins = "http://localhost:4200")
public class GateWayAPIs {

	@Autowired
	NoneUserServiceProxy noneUserServiceProxy;

	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtProvider jwtProvider;

	@PostMapping("/login")
	public DataReturnOne<JwtResponseDto> login(@RequestBody UserDto login) {
		DataReturnOne<JwtResponseDto> dataReturnOneJwt = new DataReturnOne<JwtResponseDto>();
		DataReturnOne<User> dataReturnOne = new DataReturnOne<User>();
		bCryptPasswordEncoder = new BCryptPasswordEncoder();
		dataReturnOne = noneUserServiceProxy.checkUser(login.getEmail());		
		if (dataReturnOne.getSuccess().equals("false")) {
			dataReturnOneJwt.setData(null);
			dataReturnOneJwt.setMessage(dataReturnOne.getMessage());
			dataReturnOneJwt.setSuccess("false");
		} else {
			User user = new User();
			user = dataReturnOne.getData();					
			if (!bCryptPasswordEncoder.matches(login.getPassword(), user.getPassword())) {
				dataReturnOneJwt.setData(null);
				dataReturnOneJwt.setMessage("Mật khẩu nhập vào không đúng");
				dataReturnOneJwt.setSuccess("false");
			} else {
				Authentication authentication = authenticationManager
						.authenticate(new UsernamePasswordAuthenticationToken(login.getEmail(), login.getPassword()));

				SecurityContextHolder.getContext().setAuthentication(authentication);

				String jwt = jwtProvider.generateJwtToken(authentication);
				UserPrinciple userDetails = (UserPrinciple) authentication.getPrincipal();
				JwtResponseDto jwtRes = new JwtResponseDto(user.getLastName(), jwt, "Bearer", userDetails.getEmail(),
						userDetails.getAuthorities());
				dataReturnOneJwt.setData(jwtRes);
				dataReturnOneJwt.setMessage("Đăng nhập thành công");
			}
		}
		return dataReturnOneJwt;
	}
	
	@PostMapping("/register")
	public DataReturnOne<UserDto> registerUser(@RequestBody User user){
		bCryptPasswordEncoder = new BCryptPasswordEncoder();
		DataReturnOne<UserDto> userDataReturnOne = new DataReturnOne<UserDto>();
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userDataReturnOne = noneUserServiceProxy.register(user);
		return userDataReturnOne;
	}
	
	

}
