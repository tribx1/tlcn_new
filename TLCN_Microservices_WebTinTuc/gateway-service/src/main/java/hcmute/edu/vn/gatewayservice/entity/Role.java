package hcmute.edu.vn.gatewayservice.entity;


import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor

public class Role {

    
    private long Id;

    private String rname;

    private Boolean p_create;

    private Boolean p_update;

    private Boolean p_delete;

    private Boolean p_approve;

    private Boolean p_admin;

    private int status;

    private Date dateCreated;

    private String userCreated;

    private Date dateUpdated;

    private String userUpdated;

    private long CatId;
    
    private String permission;

}

