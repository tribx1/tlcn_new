package hcmute.edu.vn.gatewayservice.controller;

import hcmute.edu.vn.gatewayservice.api.v1.data.DataReturnOne;
import hcmute.edu.vn.gatewayservice.api.v1.dto.PasswordDto;
import hcmute.edu.vn.gatewayservice.api.v1.dto.UserDto;
import hcmute.edu.vn.gatewayservice.entity.User;
import hcmute.edu.vn.gatewayservice.service.UserServiceProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController

public class UserAPIs {

    @Autowired
    UserServiceProxy userServiceProxy;
    
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @GetMapping("/profile/{email}")
    public DataReturnOne<User> Profile(@PathVariable("email") String email){
        DataReturnOne<User> dataReturnOne = new DataReturnOne<>();
        dataReturnOne = userServiceProxy.Profile(email);
        return dataReturnOne;
    }
    
    @PostMapping("/updatePass")
    public DataReturnOne<UserDto> editPassWord(@RequestBody PasswordDto passwordDto){
    	DataReturnOne<User> dataReturnOne = new DataReturnOne<>();
    	DataReturnOne<UserDto> dataReturnOneNew = new DataReturnOne<>();
        dataReturnOne = userServiceProxy.Profile(passwordDto.getEmail());
        if(dataReturnOne.getSuccess().equals("false")) {
        	dataReturnOneNew.setData(null);
        	dataReturnOneNew.setMessage("Email not found");
        	dataReturnOneNew.setSuccess("false");
        	return dataReturnOneNew;
        }
        bCryptPasswordEncoder = new BCryptPasswordEncoder();
        if(!bCryptPasswordEncoder.matches(passwordDto.getOldPassword(), dataReturnOne.getData().getPassword())) {
        	dataReturnOneNew.setData(null);
        	dataReturnOneNew.setMessage("Password not correct !!!!");
        	dataReturnOneNew.setSuccess("false");
        	return dataReturnOneNew;
        } else {
        	passwordDto.setNewPassword(bCryptPasswordEncoder.encode(passwordDto.getNewPassword()));
        	dataReturnOneNew = userServiceProxy.editPassWord(passwordDto);
        }
    	return dataReturnOneNew;
    	
    }

}
