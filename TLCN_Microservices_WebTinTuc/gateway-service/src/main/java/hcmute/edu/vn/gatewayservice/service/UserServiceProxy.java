package hcmute.edu.vn.gatewayservice.service;

import hcmute.edu.vn.gatewayservice.api.v1.data.DataReturnOne;
import hcmute.edu.vn.gatewayservice.api.v1.dto.PasswordDto;
import hcmute.edu.vn.gatewayservice.api.v1.dto.UserDto;
import hcmute.edu.vn.gatewayservice.entity.User;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
@Service
@FeignClient(name="user-service" )
@RequestMapping(value = "/user")
public interface UserServiceProxy {
    @GetMapping("/profile/{email}")
    public DataReturnOne<User> Profile(@PathVariable("email") String email);
    
    @PostMapping("/updatePass")
    public DataReturnOne<UserDto> editPassWord(@RequestBody PasswordDto passwordDto);
}
