package hcmute.edu.vn.adminservice.service.impl;

import hcmute.edu.vn.adminservice.model.Cat;
import hcmute.edu.vn.adminservice.model.Cat_Item_Count;
import hcmute.edu.vn.adminservice.repository.CatItemCountRepository;
import hcmute.edu.vn.adminservice.repository.CatRepository;
import hcmute.edu.vn.adminservice.service.CatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CatServiceImpl implements CatService {

    @Autowired
    CatRepository catRepository;

    @Autowired
    CatItemCountRepository catItemCountRepository;

    @Override
    public CrudRepository<Cat, Long> getRepo() {
        return catRepository;
    }

    @Override
    public  List<Cat> retrieveAllCat() {
        return catRepository.findAll();
    }

    @Override
    public List<Cat> retrieveAllCatChecked() {
        int value = 1;
        return catRepository.findCatByCheckCat(value);
    }

    @Override
    public Cat retrieveCatById(long id) {
        return catRepository.findById(id).get();
    }

    @Override
    public Cat UpdateCategory(Cat cat) {
        Cat catUpdate = catRepository.findById(cat.getId()).get();
        catUpdate.setName(cat.getName());
        return catRepository.save(catUpdate);
    }
    
    @Override
    public Cat InsertCategory(Cat cat) {
        return catRepository.save(cat);
    }

    @Override
    public void DeleteCategory(long id) {
        Cat cat = catRepository.findById(id).get();
        if(cat != null){
            cat.setCheckCat(0);
            catRepository.save(cat);
        }
    }
    
    @Override
    public List<Cat_Item_Count> retrieveAllItemGroupByCatId() {
        List<Object[]> temps = catItemCountRepository.countItemById();
        List<Cat_Item_Count> cat_item_counts = new ArrayList<>();
        for (int i = 0; i < temps.size(); i++) {
            Cat_Item_Count temp = new Cat_Item_Count();
            temp.setLabel(String.valueOf(temps.get(i)[0]));
            temp.setValue(temps.get(i)[1].toString());
            cat_item_counts.add(temp);
        }
    	return cat_item_counts;
    }
}
