package hcmute.edu.vn.adminservice.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cat_Item_Count {
	
	private String label;
	
	private String value;
}
