package hcmute.edu.vn.adminservice.repository;

import hcmute.edu.vn.adminservice.model.Cat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface CatItemCountRepository extends JpaRepository<Cat, Long> {
    @Query(value = "SELECT nec.name as label, count(neci.item_id) AS value FROM ne_cats nec LEFT JOIN ne_cat_item neci" +
                    " ON nec.id = neci.cat_id GROUP BY nec.name", nativeQuery = true)
    List<Object[]> countItemById();
}

