package hcmute.edu.vn.nuservice.api.v1.dto;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRoleDto {
	private String email;
	private String password;
	private String lastName;
	private Set<String> permissionList;
}
