package hcmute.edu.vn.userservice.api.v1.dto;

import hcmute.edu.vn.userservice.model.Cat;
import hcmute.edu.vn.userservice.model.ItemAccess;
import lombok.Data;

import java.util.Set;

@Data
public class ItemDto {
    private Long id;
    private String fileName;
    private String fileExtension;
    private String image;
    private String title;
    private String shortDesc;
    private String fullDesc;
    private String author;
    private Long views;
    private Long likes;
    private String linkOrigin;
    private String originName;
    private Set<Cat> cats;
    private Set<ItemAccess> itemAccesses;
}
