import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../../_entity/user';
import { Guest } from 'src/app/_entity/guest';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenConstant } from '../../_constant/auth-constant';
@Injectable({
  providedIn: 'root'
})
export class NuServiceService {
  context = environment.base_url;
  jwtHelper: JwtHelperService;
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' })
  constructor(private http: HttpClient) {
    this.jwtHelper = new JwtHelperService
   }

  register(user: User) : Observable<any>{    
    return this.http.post(`${this.context}/register`,user);
  }
  checkEmail(email : string) : Observable<any> {
    return this.http.get(`${this.context}/api/v1/nuser/check-user/${email}`,{observe:`response`});
  }

  checkEmailPass(guest: Guest) : Observable<any> {
    return this.http.post(`${this.context}/api/v1/nuser/check-account`,guest);
  }

  login(guest: Guest) : Observable<any>{    
    return this.http.post(`${this.context}/login`,guest, { headers: this.headers });
  }

  getCatItem(itemid : number) :Observable<any> {
    return this.http.get(`${this.context}/api/v1/nuser/getcatofitem/${itemid}`);
  }

  getItemDescDay() :Observable<any> {
    return this.http.get(`${this.context}/api/v1/nuser/get-item-desc-day`);
  }

  getItemDescDayAll() :Observable<any> {
    return this.http.get(`${this.context}/api/v1/nuser/get-item-desc`);
  }

  getAllItemsPage(id: number, page: number, size: number): Observable<any>{
    return this.http.get(`${this.context}/api/v1/nuser/itemsbycat?&page=${page}&size=${size}&id=${id}`);
  }

  getAllCatChecked() :Observable<any>{
    return this.http.get(`${this.context}/api/v1/nuser/cat/checked`);
  }

  getAllParentCatChecked() :Observable<any>{
    return this.http.get(`${this.context}/api/v1/nuser/cat/parentcatchecked`);
  }

  getItemDescLike() :Observable<any> {
    return this.http.get(`${this.context}/api/v1/nuser/get-item-desc-like`);
  }

  getChildCat(id: number) :Observable<any>{
    return this.http.get(`${this.context}/api/v1/nuser/cat/childcat/${id}`);
  }

  updateViewItem(itemId): Observable<any>{
    return this.http.get(`${this.context}/api/v1/nuser/item/updateview/${itemId}`);
  }

  IsAuthenticated() {
    const token = localStorage.getItem(TokenConstant.X_ACCESS_TOKEN);
    if (!token) {
      return false;
    }
    return !this.jwtHelper.isTokenExpired(token);
  }

  Logout() {
    localStorage.clear();
  }
}
