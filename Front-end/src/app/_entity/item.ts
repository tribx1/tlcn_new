export class Item {
    id : number
    fileName : string
    fileExtension : string
    image : string
    title : string
    status : number
    shortDesc : string
    fullDesc : string
    author : string
    views : number
    likes : number   
    linkOrigin : string;
    originName : string; 
    dateCreated : Date
    userCreated : string
    dateUpdated : Date
    userUpdated : string
    isNull : boolean = false
}