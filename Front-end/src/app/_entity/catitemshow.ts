export class CatItemShow {
    catId: number
    catName: string
    itemId: number  
    title: string
    status: number
    shortDesc: string    
    author: string    
}