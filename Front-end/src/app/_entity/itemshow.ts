export class ItemShow {
    id : number  
    title : string
    status : number   
    author : string  
    isNull : boolean = false
}