import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebCatManagementComponent } from './web-cat-management.component';

describe('WebCatManagementComponent', () => {
  let component: WebCatManagementComponent;
  let fixture: ComponentFixture<WebCatManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebCatManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebCatManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
