import { Component, OnInit, ChangeDetectorRef } from '@angular/core';


import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import { ModServiceService } from '../_service/mod_service/mod-service.service';
import { Router } from '@angular/router';
import { Item } from '../_entity/item';
import { Role } from '../_entity/role';
import { first } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import { UserService } from '../_service/user_service/user.service';
import { NuServiceService } from '../_service/nu_service/nu-service.service';
import { Cat } from '../_entity/cat';
import { CatItem } from '../_entity/catitem'
import { Guest } from '../_entity/guest';
import { PageEvent } from '@angular/material/paginator';
import { ItemShow } from '../_entity/itemshow';

@Component({
  selector: 'app-cat-new-management',
  templateUrl: './cat-new-management.component.html',
  styleUrls: ['./cat-new-management.component.css']
})
export class CatNewManagementComponent implements OnInit {

  items: ItemShow[];
  dataTable: any;
  email: string;
  isMod: boolean = false;
  rolesofUser: Role[];
  cats: Cat[];
  catItems: CatItem[];
  select: number;
  catId: number;
  pass: string;
  error: string;
  guest: Guest = new Guest();
  displayedColumns: string[] = ['title', 'shortDesc', 'author', 'status'];
  dataSource = new MatTableDataSource(this.items);
  length = 100;
  pageSize = 5;
  pageIndex = 0;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  constructor(private router: Router, private modService: ModServiceService, private chRef: ChangeDetectorRef,
    private userService: UserService, private nuService: NuServiceService) { }

  ngOnInit() {

    this.getListItem()
  }

  async getListItem() {
    await this.getAllItem()
    await this.checkEmail()
  }
  getAllItem() {

    this.modService.getItemPagination( this.pageSize, this.pageIndex)
      .subscribe(res => {
        if (res.success == "true") {

          this.items = res.data;
          this.dataSource.data = this.items;
          this.length = res.totalElement;

        }
      }, err => {
        console.log(err.message)
      });

  }
  onGotoItemDetail(id) {

    // this.router.navigate(["/newsdetail/" + id]);
    window.location.href = "/newsdetail/" + id;
  }

  onChangePage(event?: PageEvent) {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.modService.getItemPagination(event.pageSize, event.pageIndex)
      .subscribe(res => {
        if (res.success == "true") {
          this.items = res.data;
          this.dataSource.data = this.items;
          this.length = res.totalElement;
        }
      }, err => {
        console.log("err.message");
      });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  checkEmail() {

    this.email = localStorage.getItem("email")
    if (this.email == null) {
      // this.router.navigate(["/"])
      window.location.href = "/";
    }
    this.userService.getProfile(this.email)
      .pipe(first())
      .subscribe(res => {
        if (res.success == "true") {
          this.rolesofUser = res.data.roles
          console.log(this.rolesofUser)
          for (let role of this.rolesofUser) {
            if (role.p_update == true || role.permission == ("ROLE_ADMIN")) {
              if (role.status == 1) {
                this.isMod = true;
                return
              }

            }

          }
          if (this.isMod == false) {
            alert("Bạn không được truy cập vào trang này")
            // this.router.navigate(["/"])
            window.location.href = "/";
          }
        }
        else {
          this.error = res.message
        }
      }, err => {
        console.log(err)
      })

  }


}
