import { Component, OnInit } from '@angular/core';
import { UserService } from '../_service/user_service/user.service';
import { first } from 'rxjs/operators';
import { User } from '../_entity/user';
import { NuServiceService } from '../_service/nu_service/nu-service.service';
import { Router } from '@angular/router';
import { Guest } from '../_entity/guest';

@Component({
  selector: 'app-password-edit',
  templateUrl: './password-edit.component.html',
  styleUrls: ['./password-edit.component.css']
})
export class PasswordEditComponent implements OnInit {

  email: string = "";
  oldPassword: string = "";
  newPassword: string = "";
  conFirmNewPassword: string = "";
  pass : string = "";
  selectedImg : string = "";
  user : User;
  guest : Guest;
  formEdit : any;
  error : string = ""
  constructor(private userService : UserService, private nuService : NuServiceService, private route :Router) {
    this.user = new User();
    this.guest = new Guest();
   }

  ngOnInit() {
    
    this.getProfile()
  }

  getProfile(){    
    this.email = localStorage.getItem("email")
    this.userService.getProfile(this.email)
    .pipe(first())
    .subscribe(res => {
      if(res.success == "true")
      {
        this.user = res.data;
        if(this.user.avatar == null){                   
          this.selectedImg = "/assets/robust-admin/profile.png"
        } else 
        {
          this.selectedImg = this.user.avatar
        }   
      }
      else
      {
          this.error = res.message
      }
    }, err => {
      console.log(err)
    })  
  }

  doiMatKhau(){
    this.email = localStorage.getItem("email");
    if(this.newPassword == this.conFirmNewPassword)
    {
      this.formEdit = {
        "email" : this.email,
        "oldPassword" : this.oldPassword,
        "newPassword" : this.newPassword
      }
      this.userService.editPassWord(this.formEdit).pipe(first())
      .subscribe(res =>{
          if(res.success == "true") {
            alert("Đổi mật khẩu thành công !!!");
          } else {
            alert(res.message);
          }
            
      },
      err =>{
          this.error = err.message
      });
    }
    else{
      this.error = "Mật khẩu nhập lại không khớp"
    }
  } 
    
}
