import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NuServiceService } from '../_service/nu_service/nu-service.service';
import { User } from '../_entity/user';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user : User;
  error : string;
  status : string;
  registerForm: FormGroup;
  submitted = false;
  
  constructor(private router : Router,private nuService : NuServiceService, private formBuilder: FormBuilder ) {
    this.user = new User()
    this.user.sex = 1
    this.user.status = 1
   }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required]
  }, {
      validator: MustMatch('password', 'confirmPassword')
  });
  }

  register(){
    this.submitted = true;
    console.log(this.user);
    if(this.registerForm.valid)
    {
      this.nuService.register(this.user).pipe(first()).subscribe(res => {
        console.log(res.message)
        if(res.success == "true"){
          alert("Tạo tài khoản thành công !!");
          localStorage.setItem("email", res.data.email)
          localStorage.setItem("lastName", res.data.lastName)
          localStorage.setItem("password", res.data.password)
          // this.router.navigate(["/"]);    
          window.location.href = "/";     
          this.error = "";
        }
        else{
          alert("Email đã tồn tại !!!");
        }
        
       
    },
    err => {
        this.error = err.message
    })
    }
    
   
  }
  get f() { return this.registerForm.controls; }
  checkEmail(){
    this.nuService.checkEmail(this.user.email).pipe(first()).subscribe(res => {

    },
    err => {

    })
  }

}

export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ mustMatch: true });
      } else {
          matchingControl.setErrors(null);
      }
  }
}