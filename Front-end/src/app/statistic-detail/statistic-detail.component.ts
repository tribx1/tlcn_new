import { Component, OnInit } from '@angular/core';
import { User } from '../_entity/user';
import { Router } from '@angular/router';
import { UserService } from '../_service/user_service/user.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Role } from '../_entity/role';
import { AdminService } from '../_service/admin_service/admin.service';
import { NuServiceService } from '../_service/nu_service/nu-service.service';
import { Cat } from '../_entity/cat';
import { Guest } from '../_entity/guest';

@Component({
  selector: 'app-statistic-detail',
  templateUrl: './statistic-detail.component.html',
  styleUrls: ['./statistic-detail.component.css']
})
export class StatisticDetailComponent implements OnInit {
  user: User;
  error: string;
  email: string;
  selectCat: number = 0;
  pass: string;
  role: Role;
  cats: Cat[];
  rolesofUser: Role[];
  isAdmin: boolean = false;
  guest: Guest;

  dataSource: any;
  chartConfig: Object;

  constructor(private adminService: AdminService, private fb: FormBuilder, private router: Router, private userService: UserService, private nuService: NuServiceService) {
    this.role = new Role();
    this.user = new User();
    this.guest = new Guest();

    this.chartConfig = {
      width: '700',
      height: '400',
      type: 'column2d',
      dataFormat: 'json',
    };

    this.dataSource = {
      "chart": {
        "caption": "Thống Kê Tin Tức Theo Danh Mục",
        "subCaption": "",
        "xAxisName": "Danh Mục",
        "yAxisName": "Bài viết",
        "numberSuffix": " bài",
        "theme": "gammel",
      },
      "data": [{
        "label": "",
        "value": ""
      }]
    };
  }

  ngOnInit() {
    this.getAllCat();
    this.countItem();
  }

  countItem() {
    this.adminService.countItemByCategory()
    .pipe(first())
    .subscribe(res => {
      if (res.success == "true") {
        this.dataSource.data = res.data;
      } else {
        console.log(res.message)
      }

    }, err => {
      console.log(err)
    })
}

  async getAllCat() {
    await this.checkEmail()
    this.adminService.getAllCat()
      .pipe(first())
      .subscribe(res => {
        if (res.success == "true") {
          this.cats = res.data;
        } else {
          console.log(res.message)
        }

      }, err => {
        console.log(err)
      })

  }

  async checkEmail() {
    this.email = localStorage.getItem("email")
    if (this.email == null) {
      window.location.href = "/";
    }
    this.userService.getProfile(this.email)
      .pipe(first())
      .subscribe(res => {
        if (res.success == "true") {
          this.rolesofUser = res.data.roles
          for (let role of this.rolesofUser) {
            if (role.permission == ("ROLE_ADMIN") && role.status == 1) {
              this.isAdmin = true;
              return
            }
          }
          if (this.isAdmin == false) {
            alert("Bạn không được truy cập vào trang này")
            window.location.href = "/";
          }
        }
        else {
          this.error = res.message
        }
      }, err => {
        console.log(err)
      })
  }
}
