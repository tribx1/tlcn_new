import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { TokenConstant } from '../_constant/auth-constant';


@Injectable()
export class HttpAuthInterceptor implements HttpInterceptor {
  constructor() { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let currentUser = localStorage.getItem(TokenConstant.X_ACCESS_TOKEN);     
    if (currentUser) {
      request = request.clone({        
        setHeaders: {         
          Authorization : `Bearer ${currentUser}`       
        }
      });     
    }    
    return next.handle(request);
  }

}
