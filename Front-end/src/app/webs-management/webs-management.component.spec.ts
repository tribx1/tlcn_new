import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebsManagementComponent } from './webs-management.component';

describe('WebsManagementComponent', () => {
  let component: WebsManagementComponent;
  let fixture: ComponentFixture<WebsManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebsManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebsManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
