import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';


import * as $ from 'jquery';

import { ModServiceService } from '../_service/mod_service/mod-service.service';
import { Router } from '@angular/router';
import { Item } from '../_entity/item';
import { Role } from '../_entity/role';
import { first } from 'rxjs/operators';
import { UserService } from '../_service/user_service/user.service';
import { NuServiceService } from '../_service/nu_service/nu-service.service';
import { Cat } from '../_entity/cat';
import { CatItem } from '../_entity/catitem';
import { Guest } from '../_entity/guest';
import { MatTableDataSource } from '@angular/material/table';
import { PageEvent } from '@angular/material/paginator';
import { CatItemShow } from '../_entity/catitemshow';
import { AdminService } from '../_service/admin_service/admin.service';
import { Web } from '../_entity/web';

@Component({
  selector: 'app-webs-management',
  templateUrl: './webs-management.component.html',
  styleUrls: ['./webs-management.component.css']
})
export class WebsManagementComponent implements OnInit {

  items: Item[];
  dataTable: any;
  title: String = "";
  url: String = "";
  classContent: String = "";
  id: number = 0;
  email: string
  isMod: boolean = false
  rolesofUser: Role[]
  webs: Web[]
  web: Web = new Web();
  catItems: CatItemShow[]
  select: number
  catId: number
  pass: string
  error: string
  isAdmin: boolean = false
  guest: Guest = new Guest();
  displayedColumns: string[] = ['title', 'url', 'classContent', 'delete'];
  dataSource = new MatTableDataSource(this.webs);
  length = 100;
  pageSize = 5;
  pageIndex = 0;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  constructor(private router: Router, private adminService: AdminService, private chRef: ChangeDetectorRef,
    private userService: UserService, private nuService: NuServiceService) { }

  ngOnInit() {
    this.checkEmail()
  }


  onSlect(id, title, url, classContent) {
    this.id = id;
    this.title = title;
    this.url = url;
    this.classContent = classContent;
  }

  onChangePage(event?: PageEvent) {
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
    this.adminService.getWebPagination(this.pageSize, this.pageIndex)
      .subscribe(res => {
        if (res.success == "true") {
          this.webs = res.data;
          this.dataSource.data = this.webs;
          this.length = res.totalElement;
        }
      }, err => {
        console.log(err.message)
      });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  checkEmail() {
    this.email = localStorage.getItem("email")
    if (this.email == null) {
      // this.router.navigate(["/"])
      window.location.href = "/";
    }
    this.userService.getProfile(this.email)
      .pipe(first())
      .subscribe(res => {
        if (res.success == "true") {
          this.rolesofUser = res.data.roles
          for (let role of this.rolesofUser) {
            if (role.permission == ("ROLE_ADMIN") && role.status == 1) {
              this.isAdmin = true
            }
          }
          if (this.isAdmin == false) {

            alert("Bạn không được truy cập vào trang này")
            // this.router.navigate(["/"])
            window.location.href = "/";

          } else {
            if (this.isAdmin == true) {
              this.adminService.getWebPagination(this.pageSize, this.pageIndex)
                .subscribe(res => {
                  if (res.success == "true") {
                    this.webs = res.data;
                    this.dataSource.data = this.webs;
                    this.length = res.totalElement;
                  }
                }, err => {
                  console.log(err.message)
                });

            }
          }
        }
        else {
          this.error = res.message
        }
      }, err => {
        console.log(err)
      })

  }

  createWeb() {
    this.web.title = this.title;
    this.web.url = this.url;
    this.web.classContent = this.classContent;
    if (this.title == "" || this.url == "" || this.classContent == "") {
      alert("Mời nhập đầy đủ thông tin");
    } else {
      this.adminService.createWeb(this.web).subscribe(res => {
        if (res.success == "true") {
          alert("Thêm web thành công");
          this.adminService.getWebPagination(this.pageSize, this.pageIndex)
            .subscribe(res => {
              if (res.success == "true") {
                this.webs = res.data;
                this.dataSource.data = this.webs;
                this.length = res.totalElement;
              }
            }, err => {
              console.log(err.message)
            });
          this.title = "";
          this.url = "";
          this.classContent = "";
        } else {
          alert("Thêm web không thành công");
        }
      }, err => {
        console.log(err)
      });
    }

  }

  deleteWeb(webId) {
    this.adminService.deleteWeb(webId).subscribe(res => {
      if (res.success == "true") {
        alert("Xóa Web thành công");
        this.adminService.getWebPagination(this.pageSize, this.pageIndex)
          .subscribe(res => {
            if (res.success == "true") {
              this.webs = res.data;
              this.dataSource.data = this.webs;
              this.length = res.totalElement;
            }
          }, err => {
            console.log(err.message)
          });
        this.title = "";
        this.url = "";
        this.classContent = "";
      } else {
        alert(res.message);
      }
    }, err => {
      console.log(err)
    });
  }

  updateWeb() {
    this.web.title = this.title;
    this.web.url = this.url;
    this.web.classContent = this.classContent;
    this.web.id = this.id;
    if(this.id == 0) {
      alert("Mời chọn web cần thay đổi");
    } else if (this.title == "" || this.url == "" || this.classContent == "") {
      alert("Mời nhập đầy đủ thông tin");
    } else {
      this.adminService.updateWeb(this.web).subscribe(res => {
        if (res.success == "true") {
          alert("Update web thành công");
          this.adminService.getWebPagination(this.pageSize, this.pageIndex)
            .subscribe(res => {
              if (res.success == "true") {
                this.webs = res.data;
                this.dataSource.data = this.webs;
                this.length = res.totalElement;
              }
            }, err => {
              console.log(err.message)
            });
          this.title = "";
          this.url = "";
          this.classContent = "";
        } else {
          alert("Update web không thành công");
        }
      }, err => {
        console.log(err)
      });
    }
  }
}

