import { Component, OnInit } from '@angular/core';
import { Guest } from '../_entity/guest';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NuServiceService } from '../_service/nu_service/nu-service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TokenConstant } from '../_constant/auth-constant'
import { UserService } from '../_service/user_service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  guest: Guest
  error: String
  loginForm: FormGroup;
  submitted = false;

  constructor(private nuService: NuServiceService, private route: Router, private formBuilder: FormBuilder, private userService: UserService) {
    this.guest = new Guest();
  }

  ngOnInit() {

    if (localStorage.email) {
      // this.route.navigate(["/"]);
      window.location.href = "/";
    } else {
      this.loginForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(6)]]
      });
    }

  }
  get f() { return this.loginForm.controls; }
  login() {
    this.submitted = true;
    if (this.loginForm.valid) {
      this.nuService.login(this.guest)
        .pipe(first())
        .subscribe(res => {
          if (res.success == "true") {
            console.log(res)
            localStorage.setItem("email", res.data.email);
            localStorage.setItem("lastName", res.data.lastName);
            localStorage.setItem(TokenConstant.X_ACCESS_TOKEN, res.data.token);
            window.location.href = "/";
          }
          else {
            this.error = res.message
          }
        }, err => {
          console.log(err)
        })
    }

  }
  logout() {
    localStorage.clear()
  }
}
