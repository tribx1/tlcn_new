import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../_service/user_service/user.service';
import { NuServiceService } from '../_service/nu_service/nu-service.service';
import { ModServiceService } from '../_service/mod_service/mod-service.service';
import { User } from '../_entity/user';
import { first } from 'rxjs/operators';
import { Role } from '../_entity/role';
import { Cat } from '../_entity/cat';
import { parentCat } from '../_entity/parentcat';

@Component({
  selector: 'app-cat-menu',
  templateUrl: './cat-menu.component.html',
  styleUrls: ['./cat-menu.component.css']
})
export class CatMenuComponent implements OnInit {

  user: User
  catList: parentCat[];
  email: string
  error: string
  isDashboard: boolean = false
  isAdmin: boolean = false
  isMod: boolean = false
  isCreate: boolean = false
  isUpdate: boolean = false
  isManage: boolean = false
  roles: Role[]
  constructor(private router: Router, private modService: ModServiceService, private userService: UserService, private nuService: NuServiceService) {
    this.user = new User()
  }

  ngOnInit() {


    this.getCatList();
  }

  getProfile() {
    this.email = localStorage.getItem("email");
    if (this.email) {
      this.userService.getProfile(this.email)
        .pipe(first())
        .subscribe(res => {
          if (res.success == "true") {
            this.user = res.data;
            this.roles = this.user.roles
            for (let role of this.roles) {
              if (role.permission == ("ROLE_ADMIN") && role.status == 1) {
                this.isAdmin = true;
                this.isMod = true;
                this.isCreate = true;
                this.isUpdate = true;
                this.isManage = true;
              }
              if (role.p_delete == true || role.p_approve == true || role.p_update == true) {
                if (role.status == 1) {
                  this.isMod = true;
                }

              }
              if ((role.p_delete == true || role.p_approve == true || role.p_create || role.p_update) && (role.catId == 0)) {
                if (role.status == 1) {
                  this.isManage = true;
                }

              }
              if (role.p_update == true) {
                if (role.status == 1) {
                  this.isUpdate = true;
                }

              }
              if (role.p_create == true && role.status == 1) {
                this.isCreate = true;
              }
              if (role.permission == ("ROLE_ADMIN") || this.isMod == true || this.isCreate == true || this.isUpdate == true) {
                this.isDashboard = true;
              }
            }
          }
          else {
            this.error = res.message
          }
        }, err => {
          console.log(err)
        });
    }

  }

  goTo(e) {
    console.log(e);
    switch(e) { 
      case 1: { 
        window.location.href = "/usersmanagement";
         break; 
      } 
      case 2: { 
        window.location.href = "/rolesmanagement";
         break; 
      } 
      case 3: { 
        window.location.href = "/rolecreate";
         break; 
      } 
      case 4: { 
        window.location.href = "/newsmanagement";
         break; 
      } 
      case 5: { 
        window.location.href = "/catnewmanagement";
         break; 
      } 
      case 6: { 
        window.location.href = "/catmanagement";
         break; 
      } 
      case 7: { 
        window.location.href = "/catcreate";
         break; 
      } 
      case 8: { 
        window.location.href = "/newstyping";
         break; 
      } 
      case 9: { 
        window.location.href = "/web-management";
         break; 
      } 
      case 10: { 
        window.location.href = "/web-cat-management";
         break; 
      }
      case 11: { 
        window.location.href = "/statistic-detail";
         break; 
      } 
      default: { 
        window.location.href = "/";
         break; 
      } 
   } 
   
  }

  async getCatList() {
    await this.getProfile();
    await this.nuService.getAllParentCatChecked()
      .subscribe(res => {
        if (res.success == "true") {
          this.catList = res.data;

          for (let i = 0; i < this.catList.length; i++) {
            this.nuService.getChildCat(this.catList[i].id)
              .subscribe(res => {

                if (res.success == "true") {
                  this.catList[i].childCat = res.data;
                  this.catList[i].isNullChildCat = this.catList[i].childCat.length;
                }
              }, err => {
                console.log(err.message)
              });
          }
        }
      }, err => {
        console.log(err.message)
      });
  }
}

